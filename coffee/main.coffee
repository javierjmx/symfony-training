$("#form").validate(
  rules:
    name: "required"
    address: "required"
    phone: "required"
  messages:
    name:
      required: "Requerido"
    address:
      required: "Requerido"
    phone:
      required: "Requerido"
  submitHandler: (form) ->
    $form = $(form)

    loader = $("#loader")
    loader.show()

    name = $form.find('input[name="name"]').val()
    address = $form.find('input[name="address"]').val()
    phone = $form.find('input[name="phone"]').val()
    url = "/app_dev.php/blog"

    displayData = (data) ->
      $("#name").text(data.name)
      $("#address").text(address)
      $("#phone").text(phone)
      $("#info").removeClass("invisible")

    $.post(
      url,
      {name: name, address: address, phone: phone},
      (data) ->
        loader.hide()
        displayData(data)
      "json"
    )
)
