<?php

namespace Acme\OtherBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
  public function indexAction(Request $request)
  {
    $data = $request->request->all();

    if ($request->request->count() == 3) {
      return new JsonResponse($data);
    }

    return $this->render(
      'AcmeOtherBundle:Default:index.html.twig',
      array($request->request->all())
    );
  }
}
