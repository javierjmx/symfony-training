// Generated by CoffeeScript 1.7.1
(function() {
  $("#form").validate({
    rules: {
      name: "required",
      address: "required",
      phone: "required"
    },
    messages: {
      name: {
        required: "Requerido"
      },
      address: {
        required: "Requerido"
      },
      phone: {
        required: "Requerido"
      }
    },
    submitHandler: function(form) {
      var $form, address, displayData, loader, name, phone, url;
      $form = $(form);
      loader = $("#loader");
      loader.show();
      name = $form.find('input[name="name"]').val();
      address = $form.find('input[name="address"]').val();
      phone = $form.find('input[name="phone"]').val();
      url = "/app_dev.php/blog";
      displayData = function(data) {
        $("#name").text(data.name);
        $("#address").text(address);
        $("#phone").text(phone);
        return $("#info").removeClass("invisible");
      };
      return $.post(url, {
        name: name,
        address: address,
        phone: phone
      }, function(data) {
        loader.hide();
        return displayData(data);
      }, "json");
    }
  });

}).call(this);
